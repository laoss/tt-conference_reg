<?php

$router->define([
    '' => 'controllers/index.php',
    'about-you' => 'views/form2.view.php',
    'share' => 'controllers/social-buttns.php',
    'all-participants' => 'controllers/participants.php',
    'send-data' => 'controllers/form2.php',
    'register' =>  'controllers/form1.php'
]);