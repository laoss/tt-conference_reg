<?php
function send_ans( $ans ) {
    $ret[ 'status' ] = $ans;    
    echo json_encode( $ret );
	  exit;
}

session_start();
if( ! empty( $_POST ) ) {    
    if( empty( $_POST[ 'firstname' ] ) ) {
		   send_ans( -1 );		
    }

    if( empty( $_POST[ 'lastname' ] ) ) {
		    send_ans( -2 );
    }

    if( empty( $_POST[ 'birthdate' ] ) ) {
		      send_ans( -3 );
    }

    if( empty( $_POST[ 'reportSubject' ] ) ) {
		      send_ans( -4 );
    }

    if( empty( $_POST[ 'country' ] ) ) {
		    send_ans( -5 );
    }

    if( empty( $_POST[ 'phone' ] ) ) {
		    send_ans( -6 );
    } //else if ( preg_match( "/^[+][1]{1}\\s[(][0-9]{3}[)]\\s[0-9]{3}-[0-9]{4}$/", $_POST[ 'phone' ] ) ) {
    //     send_ans( -7 );
    // }    
    
    if( empty( $_POST[ 'email' ] ) ) {
		    send_ans( -8 );
    }

    require "models/participant.php";
    if( !class_exists( "model\Participant" ) ) {
      send_ans( 0 );
    }    

    try {
      $participant = new model\Participant($app['pdo']);
      $email_free = $participant->isEmailFree( $_POST[ 'email' ] ); 
    } catch ( Exception $ex ) {
      send_ans( 0 );
    }
    
    if( ! $email_free ) {
      send_ans( -9 );
    }

    
    $part_data = [
      'first_name'     => $_POST['firstname'],
      'last_name'      => $_POST['lastname'],
      'birthdate'      => $_POST['birthdate'],
      'report_subject' => $_POST['reportSubject'],
      'country'        => $_POST['country'],
      'phone'          => $_POST['phone'],
      'email'          => $_POST['email']
    ];

    try {
      $usr_id = $participant->add_participant( $part_data );
    } catch ( Exception $ex ) {
      send_ans( 11 );
    }

    $_SESSION['user_id'] = $usr_id;
    $_SESSION['active_form'] = 2;
    //session_write_close();
    send_ans( 1 );
} else {
    session_unset();
    require "controllers/index.php";
    exit;
}



