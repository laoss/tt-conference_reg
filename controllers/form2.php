<?php
function send_ans( $ans ) {
    $ret[ 'status' ] = $ans;    
    echo json_encode( $ret );
	  exit;
}

//echo 'testtt';

session_start();
if( ! empty( $_POST ) ) {
    $part_data = array();
    
    if( ! empty( $_POST['company'] ) ) {
        $part_data['company'] = $_POST['company'];
    }
    if( ! empty( $_POST['position'] ) ) {
        $part_data['position'] = $_POST['position'];
    }
    if( ! empty( $_POST['aboutme'] ) ) {
        $part_data['about_me'] = $_POST['aboutme'];
    }
    
    if( ! empty( $_FILES['photo']['name'] ) ) {
        $move_status = move_uploaded_file(
            $_FILES['photo']['tmp_name'], 
            "../img/" . $_FILES['photo']['name']
        );
        if( $move_status === false ) {
            send_ans( -1 );
        } else {
            $part_data[ 'photo' ] = $_FILES['photo']['name'];
        }
    }
    
    require "../model/participant.php";
    if( ! class_exists( "model\Participant" ) ) {
        send_ans( 0 );
    }
    
    try {
        $participant = new model\Participant($pdo);
        $res = $participant->update( $part_data, $_SESSION['user_id'] );
    } catch( Exception $ex ) {
        send_ans( 0 );
    }
    
    if( $res === false ) {
        send_ans( -2 );
    }
    $_SESSION['active_form'] = 1;
    send_ans( 1 );
} else {
    $_SESSION['active_form'] = 1;
    session_unset();
    send_ans( 1 );    
}
