<?php require("views/partials/head.php") ?>

<div style="margin: 30px">
    <form >
      <div class="form-group">
        <label for="companyInput">Company</label>
        <input type="text" class="form-control" id="companyInput" name="company">   
      </div>
      <div class="form-group">
        <label for="positionInput">Position</label>
        <input type="text" class="form-control" id="positionInput" name="position">   
      </div>
      <div class="form-group">
        <label for="aboutmeTextarea">About me</label>
        <textarea class="form-control" id="aboutmeTextarea" name="aboutme" rows="3" ></textarea>
      </div>
      
      <label>Photo</label>
      <div class="custom-file mb-3">
        <input type="file" class="custom-file-input" id="photoInput">
        <label class="custom-file-label" for="photoInput">Choose file...</label>    
      </div>      
      <span id="messenger"></span>
      <button formmethod="post" style="float: right; margin-top: 30px" class="btn btn-primary" >Next</button>
    </form>
</div>

<script>
  const msg = document.getElementById( "messenger" );
  const form = document.querySelector('form');

   form.addEventListener('submit', event => {
     event.preventDefault();
    
     let company = document.getElementById('companyInput').value;
     let position = document.getElementById('positionInput').value;
     let aboutMe = document.getElementById('aboutmeTextarea').value;
     let photo = document.getElementById('photoInput');
    
    if( window.FormData !== undefined ) {
      var data = new FormData();
      let exit = true;

      if( company != '' ) {
        data.append("company", company);
        exit = false;
      }
      if( position != '' ) {
        data.append("position", position);        
        exit = false;
      }
      if( aboutMe != '' ) {
        data.append("aboutme", aboutMe);
        exit = false;
      }
      if( photo.value != '' ) {
        data.append("photo", photo.files[0]);
        exit = false;
      }

      // if(exit) {
      //   location.replace("social_butt.php");
      //   return;
      // }

      var x = new XMLHttpRequest();
      x.open("POST", "/tt-conference_reg/send-data", true);
      console.log('hello');

      x.send(data);
      console.log('world');
      x.onreadystatechange = () => {
        if( x.readyState == 4 && x.status == 200) {
          console.log(x.responseText);
          var res = JSON.parse(x.responseText);
          
				  switch( res['status'] ) {
              case 1:
                location.replace("/tt-conference_reg/share");
                break;
              case -1:
                msg.innerText = "Problem with saving photo";
                break;
              case -2:
                msg.innerText = "Problem with saving data";
                break;
              default:
              msg.innerText = "Something was wrong...";
          }
        }
      }
      x.onerror = () => {
        alert("Error!");
      }
    }
  })


  document.querySelector('.custom-file-input').addEventListener('change',function(e){
    var fileName = document.getElementById("photoInput").files[0].name;
    var nextSibling = e.target.nextElementSibling
    nextSibling.innerText = fileName
  })
</script>
</body>
</html>