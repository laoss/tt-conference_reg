
<?php require("partials/head.php") ?>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">Photo</th>
      <th scope="col">Name</th>
      <th scope="col">Report subject</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
      <?php foreach( $participants as $p ) {?>
        <tr>
            <td>
                <img class='photo' src="img/<?= ! $p['photo'] || $p['photo'] == '' ? 'no_ava.jpg' : $p['photo'] ?>" />
            </td>
            <td><?=$p['first_name'] . '  ' . $p['last_name']?></td>
            <td><?=$p['report_subject']?></td>
            <td>
                <a href="mailto:<?=$p['email']?>"><?=$p['email']?></a>
            </td>
        </tr>
      <?php }?>
  </tbody>
</table>


<?php require("partials/footer.php"); ?>