<?php 
namespace model;
use PDO;
//use PDOException;

class Participant{
    public $id;
    public $first_name;
    public $last_name;
    public $birthdate;
    public $report_subject;
    public $country;
    public $phone;
    public $email;
    public $company;
    public $position;
    public $about_me;
    public $photo;

    function __construct($pdo){
        
        $this->DB = $pdo;
    }
        

    function load_from_array( $data ) {
		if( ! is_array( $data ) ) return false ;
		if( isset( $data['first_name']     ) ) $this->first_name     = $data['first_name'];
		if( isset( $data['last_name']      ) ) $this->last_name      = $data['last_name'];
		if( isset( $data['birthdate']      ) ) $this->birthdate      = $data['birthdate'];
		if( isset( $data['report_subject'] ) ) $this->report_subject = $data['report_subject'];
		if( isset( $data['country']        ) ) $this->country        = $data['country'];
		if( isset( $data['phone']          ) ) $this->phone          = $data['phone'];
		if( isset( $data['email']	       ) ) $this->email          = $data['email'];			
		if( isset( $data['company']	       ) ) $this->company        = $data['company'];			
        if( isset( $data['position']	   ) ) $this->position       = $data['position'];			
        if( isset( $data['about_me']	   ) ) $this->about_me       = $data['about_me'];			
		if( isset( $data['photo']	       ) ) $this->photo          = $data['photo'];			

	}

    function add_participant($data) {
        if( empty($this->DB) ) return false;

        if( is_array( $data ) ) {
			$this->load_from_array( $data );
		}

        $sql = "INSERT INTO participants(
                first_name, last_name, birthdate, report_subject, country, phone, email)
        VALUES(?, ?, ?, ?, ?, ?, ?)";
        
        $prepared = $this->DB->prepare( $sql );

        $prepared->execute( [
            $this->first_name,
            $this->last_name,
            $this->birthdate,
            $this->report_subject,
            $this->country,
            $this->phone,
            $this->email
        ] );

        $query = "SELECT id FROM participants WHERE email = '$this->email'";
        $answer = $this->DB->query($query);

        $userdata = $answer->fetch(PDO::FETCH_ASSOC);
        if(empty($userdata)){
            return false;
        }

        return $userdata['id'];
    }

    function update( $data, $id ) {
        if( empty( $this->DB ) ) return false;
		
		if( is_array( $data ) ) {
			$this->load_from_array( $data );
        }
        return $this->DB->query(
            "UPDATE participants SET
            company  = '" . $this->company  . "',
            position = '" . $this->position . "',
            about_me = '" . $this->about_me . "',
            photo    = '" . $this->photo    . "'
            WHERE id = "  . $id          
        );
    }

    function isEmailFree( $email ) {
        if( empty( $this->DB ) ) return false;

        $query = "SELECT COUNT(id)
                FROM participants
                WHERE email = '$email'";
        $answer = $this->DB->query($query);
        $n = ($answer->fetch(PDO::FETCH_NUM))[0];
        return $n == 0;
    }

}