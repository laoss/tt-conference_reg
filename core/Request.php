<?php 
namespace core;

class Request
{
    public static function uri()
    {
        return trim( str_replace( 'tt-conference_reg/', '', $_SERVER['REQUEST_URI'] ), '/' );
    }
}